﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
    public delegate void SetScore();
    public static event SetScore OnSetScore;
    public Text timer;
    public Text score;
    private float startTime;
    private int scoreCount;
    private static Game instance;
    public static Game Instance{get{return instance;}}

    public Transform enemySpawnerPrefab;
    private Transform enemySpawner;
    public Transform powerUpSpawnerPrefab;
    private Transform powerUpSpawner;
    public Transform EnemyPlace;
    public Transform PowerUpPlace;
    public InputField nameField;
    public Button startButton, leaderBoardButton, restartButton, mainMenuButton, mainMenuFromLeaderboard;
    public GameObject startUI, gameUI, gameOverUI, leaderboardUI;
    private bool gameOver;
    private bool scoreBoost;
    public bool GameOver{get{return gameOver;}}

    void Awake(){
        instance = this;
        startUI.SetActive(true);
        gameUI.SetActive(false);
        gameOverUI.SetActive(false);
        leaderboardUI.SetActive(false);
    }

    [SerializeField]
    private HighscoreTable leaderboardGO;
    [SerializeField]
    private GameSettings gameSettings;
    public GameSettings GameSettings{get{return gameSettings;}}
    // Start is called before the first frame update
    
    private void OnEnable() {
        Movement.OnDead += EndGame;
        Movement.OnScoreBoost += SetScoreMulti;
    }
    void Start()
    {
        gameOver = false;
        startButton.onClick.AddListener(StartGame);
        restartButton.onClick.AddListener(ReloadGame);
        mainMenuButton.onClick.AddListener(MainMenu);
        leaderBoardButton.onClick.AddListener(Leaderboard);
        mainMenuFromLeaderboard.onClick.AddListener(MainMenu);

    }

    // Update is called once per frame
    void Update()
    {
        if(gameOver){
            CancelInvoke("AddScore");
            return;
        }
        float t = Time.time - startTime;
        string minutes = ((int)t/60).ToString();
        string seconds = ((int)t%60).ToString();
        timer.text = minutes + ":" + seconds;
    }

    void AddScore(){

        score.text = scoreCount.ToString();
        if(!scoreBoost){
        scoreCount += gameSettings.ScoreCount;
        }else
        {
            scoreCount += gameSettings.ScoreCount*gameSettings.ScoreMultiplier;
        }

    }

    private void SetScoreMulti(){
        StartCoroutine(SetScoreBoost());
    }

    private IEnumerator SetScoreBoost(){
        scoreBoost = true;
        yield return new WaitForSeconds(Game.Instance.GameSettings.ScoreMultiplierDuration);
        scoreBoost = false;
    }

    void StartGame(){

        ResetParams();
    }

    void EndGame(){

        gameOver = true;
        nameField.gameObject.SetActive(true);
        var nameInput = new InputField.SubmitEvent();
        nameInput.AddListener(SubmitName);
        nameField.onEndEdit = nameInput;
    }

    void SubmitName(string arg0){

        leaderboardGO.AddHighscoreEntry(scoreCount, arg0);
        if(OnSetScore!=null)
            OnSetScore();
        gameUI.SetActive(false);
        gameOverUI.SetActive(true);
    }

    void ReloadGame(){

        ResetParams();
        gameOver = false;
    
    }

    void MainMenu(){
        
        startUI.SetActive(true);
        gameOverUI.SetActive(false);
        leaderboardUI.SetActive(false);
        gameOver = false;

    }

    void Leaderboard(){

        startUI.SetActive(false);
        gameOverUI.SetActive(false);
        leaderboardUI.SetActive(true);

    }

    void ResetParams(){

            DestroyEnemies();
            startUI.SetActive(false);
            gameUI.SetActive(true);
            nameField.gameObject.SetActive(false);
            leaderboardUI.SetActive(false);
            gameOverUI.SetActive(false);
            SpawnEnemies();
            SpawnPowerUPs();
            startTime = Time.time;
            scoreCount = 0;
            InvokeRepeating("AddScore",0f, gameSettings.ScoreTime);
    }

    void SpawnEnemies(){

        enemySpawner = Instantiate(enemySpawnerPrefab,EnemyPlace.transform.position, EnemyPlace.transform.rotation, EnemyPlace.transform);
    }

    void SpawnPowerUPs(){

        powerUpSpawner = Instantiate(powerUpSpawnerPrefab,PowerUpPlace.transform.position, PowerUpPlace.transform.rotation, PowerUpPlace.transform);
    }

    void DestroyEnemies(){

        int i = 0;
        GameObject[] allChildren = new GameObject[EnemyPlace.transform.childCount];

        foreach (Transform child in EnemyPlace.transform)
        {
            allChildren[i] = child.gameObject;
            i += 1;
        }

        foreach (GameObject child in allChildren)
        {
            Destroy(child.gameObject);
        }
    }
    private void OnDisable() {
        Movement.OnDead -= EndGame;
        Movement.OnScoreBoost -= SetScoreMulti;
    }

    
}
