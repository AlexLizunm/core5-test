﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GameSettings")]
public class GameSettings : ScriptableObject
{
    [SerializeField]
    private int speed;
    public int Speed{get{return speed;}}

    [SerializeField]
    private int speedMultiplier;
    public int SpeedMultiplier{get{return speedMultiplier;}}

    [SerializeField]
    private int scoreMultiplier;
    public int ScoreMultiplier{get{return scoreMultiplier;}}

    [SerializeField]
    private int speedMultiplierDuration;
    public int SpeedMultiplieDuration{get{return speedMultiplierDuration;}}

    [SerializeField]
    private int scoreMultiplierDuration;
    public int ScoreMultiplierDuration{get{return scoreMultiplierDuration;}}

    [SerializeField]
    private int immortDuration;
    public int ImmortDuration{get{return immortDuration;}}

    [SerializeField]
    private int speedEnemy;
    public int SpeedEnemy{get{return speedEnemy;}}

    [SerializeField]
    private float speedEnemyMultiply;
    public float SpeedEnemyMultiply{get{return speedEnemyMultiply;}}

    [SerializeField]
    private int speedEnemyMultiplyTime;
    public int SpeedEnemyMultiplyTime{get{return speedEnemyMultiplyTime;}}

    [SerializeField]
    private int enemyGenerationCount;
    public int EnemyGenerationCount{get{return enemyGenerationCount;}}

    [SerializeField]
    private int enemyGenerationTime;
    public int EnemyGenerationTime{get{return enemyGenerationTime;}}

    [SerializeField]
    private int scoreCount;
    public int ScoreCount{get{return scoreCount;}}

    [SerializeField]
    private int scoreTime;
    public int ScoreTime{get{return scoreTime;}}

   
}
