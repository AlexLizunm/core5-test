﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpController : MonoBehaviour
{
    private Vector3 borders;
    public List <Transform> PowerUpPrefabs = new List<Transform>();
    public int firstTimeSpawPowerUp, RepeatTimePowerUp;
    void Start()
    {
        borders = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
        InvokeRepeating("SpawnPowerUp", firstTimeSpawPowerUp, RepeatTimePowerUp);
    }

    void Update() {
        if(Game.Instance.GameOver){
            CancelInvoke("SpawnPowerUp");
        }    
    }

    private void SpawnPowerUp(){

        Transform powerUpClone;  
        float x = Random.Range(borders.x, -borders.x);
        float y = Random.Range(borders.y, -borders.y);
        transform.position = new Vector3(x, y, 0);
        powerUpClone = Instantiate(PowerUpPrefabs[Random.Range(0,PowerUpPrefabs.Count)], this.transform.position, this.transform.rotation,Game.Instance.EnemyPlace.transform);        
        
    }
  
}
