﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    // Start is called before the first frame update
    private Vector3 borders;
    public Transform enemyPrefab;
    void Start()
    {
        borders = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
        InvokeRepeating("SpawnEnemy", 2f, Game.Instance.GameSettings.EnemyGenerationTime/Game.Instance.GameSettings.EnemyGenerationCount);
    }

    void Update() {
        if(Game.Instance.GameOver){
            CancelInvoke("SpawnEnemy");
        }    
    }

    private void SpawnEnemy(){

        Transform enemyClone;  
        float x = Random.Range(borders.x, -borders.x);
        float y = Random.Range(borders.y, -borders.y);
        transform.position = new Vector3(x, y, 0);
        enemyClone = Instantiate(enemyPrefab, this.transform.position, this.transform.rotation,Game.Instance.EnemyPlace.transform);        
        
    }
}
