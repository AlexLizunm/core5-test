﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private Transform target;
    private float speed;

    void Start()
    {
        speed = Game.Instance.GameSettings.SpeedEnemy;
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        InvokeRepeating("Acceleration", Game.Instance.GameSettings.SpeedEnemyMultiplyTime,Game.Instance.GameSettings.SpeedEnemyMultiplyTime);
    }

    void Update()
    {
         if(!Game.Instance.GameOver){
            StartCoroutine(Chase(speed));
         }
    }

    private IEnumerator Chase(float speed){
        
        yield return new WaitForSeconds(1f);
        transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);

    }

     private void Acceleration(){
        speed *= Game.Instance.GameSettings.SpeedEnemyMultiply;
    
    }
}
