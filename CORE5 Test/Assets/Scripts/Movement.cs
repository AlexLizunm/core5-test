﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public delegate void DeathAction();
    public static event DeathAction OnDead;
    public delegate void ScoreBoostAction();
    public static event ScoreBoostAction OnScoreBoost;
    private Vector3 target;
    private float speedBoost;
    private bool isImmortal = false;
    
    private void Start() {
        speedBoost = 1f;
    }
    void Update()
    {
        if(Input.GetMouseButtonDown(0) && !Game.Instance.GameOver){
            target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
            MoveToCursor();
    }

    private void OnCollisionEnter2D (Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            if(!isImmortal){
                
                if(OnDead!=null)
                    OnDead();
            }
             else
            {
                Destroy(collision.gameObject);      
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        switch(other.tag)
        {

            case "MultiSpeedPU":
               StartCoroutine(SetSpeedBoost());
                break;

            case "ImmortalPU":
                StartCoroutine(SetImmortal());
                break;

            case "MultiScorePU":
               if(OnScoreBoost!=null)
                    OnScoreBoost();
                break;

            default: return;
        }

        Destroy(other.gameObject);
    }

    private void MoveToCursor(){
        
        transform.position = Vector2.MoveTowards(transform.position, target, Game.Instance.GameSettings.Speed * speedBoost *Time.deltaTime);
    }

    private IEnumerator SetSpeedBoost(){
        speedBoost = Game.Instance.GameSettings.SpeedMultiplier;
        yield return new WaitForSeconds(Game.Instance.GameSettings.SpeedMultiplieDuration);
        speedBoost = 1f;
    }

    private IEnumerator SetImmortal(){
        isImmortal = true;
        yield return new WaitForSeconds(Game.Instance.GameSettings.ImmortDuration);
        isImmortal = false;
        
    }
    
    private void OnDisable() {
        transform.position = Vector3.zero;
        speedBoost = 1f;
        target = Vector3.zero;
    }
}
